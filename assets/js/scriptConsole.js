//variables boutton


//variables progress
const needDonnut = document.getElementById('needDonnut');
let hunger = needDonnut.value;

const needBeer = document.getElementById('needBeer');
let thirst = needBeer.value; 

const needSleep = document.getElementById('needSleep');
let tired = needSleep.value;

const rate = 10;

//variables images



// progressbar timer 
function hungerProgress() {
  hunger -= rate;
  //console.log(hunger);
  needDonnut.value = hunger;
  if (hunger <= 0) {
    clearInterval(timerHunger);
  }
};

function thirstProgress() {
  thirst -= rate;
  needBeer.value = thirst;
  if (thirst <= 0) {
    clearInterval(timerThirst);
  }
};

function tiredProgress() {
  tired -= rate;
  needSleep.value = tired;
  if (tired <= 0) {
    clearInterval(timerTired);
  }
};

let timerHunger = setInterval(hungerProgress, 1500);
let timerThirst = setInterval(thirstProgress, 1500);
let timerTired = setInterval(tiredProgress, 1500);


// augmenter les progressBar lignes de commandes
const check = document.getElementById('check');
const input = document.getElementById('in');
input.value = '';

function control(){
  console.log(input.value);
  switch(input.value) {
      case 'donnut':
      hunger += rate;
      console.log(hunger);
      break;
      case 'beer':
      thirst += rate;
      break;
      case 'sleep':
      tired += rate;
      break;
      default:
      alert( "I don't understand" );
      break;
  };
};
check.addEventListener('click',control);

//reset
const homerDeath = document.getElementById('homerDeath');

function death() {
  if ((hunger <= 0) && (thirst <= 0) && (tired <= 0)) {
    image.style.display = 'none';
    homerDeath.style.display='block';
  }
};


function reset(){
 location.reload()
};
let btnReset = document.getElementById('reset');
btnReset.addEventListener('click',reset);


const imageDefaut = document.getElementById('homer');
const homerEating = document.getElementById('homerEating');
const homerHappy = document.getElementById('homerHappy');
const homerSleeping = document.getElementById('homerSleeping');

//change images
function changeImage(event) {
  const value = event.target.dataset.image;
  const image = document.querySelector(`img[data-image="${value}"]`);
  if (image !== null) {
    //cacher toutes les images
    let imagesHidden = document.querySelectorAll('.img');
    for (let i = 0; i < imagesHidden.length; i++) {
      const element = imagesHidden[i];
      element.style.display = 'none'
    }
    image.style.display = 'block';

  // après 2 sec afficher images par défaut
    setTimeout(function () {
      image.style.display = 'none';
      imageDefaut.style.display = 'block'
    }, 1000);
  }
};
/*
donnut.addEventListener('click', changeImage);
beer.addEventListener('click', changeImage);
sleep.addEventListener('click', changeImage);
*/