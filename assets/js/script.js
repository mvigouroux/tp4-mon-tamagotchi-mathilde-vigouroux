// variables boutton
const donnut = document.getElementById('btnDonnut');
const beer = document.getElementById('btnBeer');
const sleep = document.getElementById('btnSleep');

// variables progress
const needDonnut = document.getElementById('needDonnut');
let hunger = needDonnut.value;

const needBeer = document.getElementById('needBeer');
let thirst = needBeer.value;

const needSleep = document.getElementById('needSleep');
let tired = needSleep.value;

const rate = 10;

// variables image
const imageDefaut = document.getElementById('homer');
const homerDeath = document.getElementById('homerDeath');
const homerEating = document.getElementById('homerEating');
const homerSleeping = document.getElementById('homerSleeping');
const homerHappy = document.getElementById('homerHappy');

/** EVENT PROGRESS BARS **/ 
function hungerProgress() {
  hunger -= rate;
  needDonnut.value = hunger;

  if (hunger <= 0) {
    clearInterval(timerHunger);
  }
}

function thirstProgress() {
  thirst -= rate;
  needBeer.value = thirst;

  if (thirst <= 0) {
    clearInterval(timerThirst);
  }
}

function tiredProgress() {
  tired -= rate;
  needSleep.value = tired;

  if (thirst <= 0) {
    clearInterval(timerTired);
  }
}

let timerHunger = setInterval(hungerProgress, 1000);
let timerThirst = setInterval(thirstProgress, 1000);
let timerTired = setInterval(tiredProgress, 1000);

// augmenter les progressBar au click
function giveDonnut() {
  hunger += rate;
}

function giveBeer() {
  thirst += rate;
}

function giveNap() {
  tired += rate;
}

donnut.addEventListener('click', giveDonnut);
beer.addEventListener('click', giveBeer);
sleep.addEventListener('click', giveNap);




// reset
function reset() {
  location.reload();
}
const btnReset = document.getElementById('reset');
btnReset.addEventListener('click', reset);


/** EVENT CHANGE IMAGES **/
function changeImage(event) {
  const value = event.target.dataset.image;
  const image = document.querySelector(`img[data-image="${value}"]`);

  if (image !== null) {
    // cacher toutes les images
    const imagesHidden = document.querySelectorAll('.img');

    for (let i = 0; i < imagesHidden.length; i++) {
      const element = imagesHidden[i];
      element.style.display = 'none';
    }

    image.style.display = 'block';

    // affiche l'image pendant une seconde
    setTimeout(() => {
      image.style.display = 'none';
      imageDefaut.style.display = 'block';
    }, 1000);
  }
}

donnut.addEventListener('click', changeImage);
beer.addEventListener('click', changeImage);
sleep.addEventListener('click', changeImage);


/** EVENT GAME OVER **/
if (hunger === 0 && thirst === 0 && tired === 0) {

  imageDefaut.style.display = 'none !important';
  homerDeath.style.display = 'block !important';
}