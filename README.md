# Application à la manière d'un Tamagotchi

## Présentation 
Dans le cadre de ma formation nous devions réaliser une page web dynamique à la manière d’un tamagotchi. C’était un projet à réaliser seul en deux semaines. 
Le projet devait comporter deux versions :
* une version où les besoins du tamagotchi devaient être remplis par des boutons;
* une deuxième version où les besoins devaient être remplis en ligne de commande.

Après une analyse du brief, j’ai réalisé une maquette où j’ai positionné les éléments demandés à savoir: 
* un espace pour voir le tamagotchi évoluer;
* des barres de progression : une pour chaque besoin (la mascotte du tamagotchi devait avoir trois besoins principaux);
* un mode pour alimenter les trois besoins défini par des boutons;
* un autre mode pour alimenter les besoins définis en ligne de commande;
* un texte d’aide rappelant les différentes règles du jeu.

L’interface est responsive. J’ai commencé par la version mobile, puis une interface pour tablette en terminant par une interface pour version desktop. 


## Moyens utilisés

J’ai utilisé le logiciel Figma pour la maquette. Le projet est  réalisé avec les langages  HTML5, Sass et JavaScript.
J’ai utilisé le framework Bootstrap CSS.
