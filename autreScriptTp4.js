//variables boutton
const donnut = document.getElementById('btnDonnut');
const beer = document.getElementById('btnBeer');
const sleep = document.getElementById('btnSleep');

//variable image par défaut
const imageDefaut = document.getElementById('homer');

//variables progress
const needDonnut = document.getElementById('needDonnut');


const needBeer = document.getElementById('needBeer');


const needSleep = document.getElementById('needSleep');

const rate = 10;

//change images
function changeImage(event) {
  const value = event.target.dataset.image;
  const image = document.querySelector(`img[data-image="${value}"]`);
  if (image !== null) {
    //cacher toutes les images
    let imagesHidden = document.querySelectorAll('.img');
    for (let i = 0; i < imagesHidden.length; i++) {
      const element = imagesHidden[i];
      element.style.display = 'none'
    }
    image.style.display = 'block';

    // après 2 sec afficher images par défaut
    setTimeout(function () {
      image.style.display = 'none';
      imageDefaut.style.display = 'block'
    }, 1000);
  }
};

donnut.addEventListener('click', changeImage);
beer.addEventListener('click', changeImage);
sleep.addEventListener('click', changeImage);


// génère progressbar timer 
function hungerProgress() {
  hunger = 100;
  hunger -= rate;
  console.log(hunger);
  needDonnut.value = hunger;
  //stocker la donnée dans le localStorage
  localStorage.setItem('hunger', hunger);
  if (hunger <= 0) {
    clearInterval(timerHunger);
    return 0;
  }
  //récupérer les données de localStorage pour la réutiliser (permet de faire ue pause)
  let hunger = localStorage.getItem('hunger');     
  needDonnut.value = hunger;
};

function thirstProgress() {
  thirst = 100;
  thirst -= rate;
  needBeer.value = thirst;
  //envoyer et stocker les données dans le localStorage
  localStorage.setItem('thirst', thirst);
  if (thirst <= 0) {
    clearInterval(timerThirst);
    return 0;
  }
  //récupérer les données de localStorage pour la réutiliser (permet de faire ue pause)
  let thirst = localStorage.getItem('thirst');
  needBeer.value = thirst;
};

function tiredProgress() {
  tired -= rate;
  needSleep.value = tired;
  //envoyer et stocker les données dans le localStorage
  localStorage.setItem('tired', tired);
  if (tired <= 0) {
    clearInterval(timerTired);
    return 0;
  }
  //récupérer les données de localStorage pour la réutiliser (permet de faire ue pause)
  let tired = localStorage.getItem('tired');
  needSleep.value = tired;
};

let timerHunger = setInterval(hungerProgress, 1000);
let timerThirst = setInterval(thirstProgress, 1000);
let timerTired = setInterval(tiredProgress, 1000);




// augmenter les progressBar au click
function giveDonnut() {
  hunger += rate;
  console.log(hunger);
  if (hunger >= 100) {

  };
};

function giveBeer() {
  thirst += rate;
  if (thirst >= 100) {

  };
};

function giveNap() {
  tired += rate;
  if (tired >= 100) {

  };
};
donnut.addEventListener('click', giveDonnut);
beer.addEventListener('click', giveBeer);
sleep.addEventListener('click', giveNap);








//reset

function alertFunc() {
  let alert = document.querySelector('.modal')
  if ((hunger <= 0) && (thirst <= 0) && (tired <= 0)) {
    alert.modal('show');
  }
};

let btnReset = document.getElementById('reset');
btnReset.addEventListener('click',reset);

function reset(){
    hunger = 100;
    needDonnut.value = hunger;
    timerHunger = setInterval(hungerProgress, 1000);

    thirst = 100;
    needBeer.value = thirst;
    timerThirst = setInterval(thirstProgress,1000);

    tired = 100;
    needSleep.value = tired;
    timerTired = setInterval(tiredProgress,1000);
};


